/**
 * Group: MET4-4
 * Author: Nikolaos Renieris
 * Description: Lexer utility functions
 */

#ifndef UTILS_H
#define UTILS_H

#include <assert.h>
#include <stdio.h>

#include "log.h"

// cast literals to char* to avoid compiler warnings
#define SPACE ' '
#define NEWLINE '\n'
#define TAB '\t'

// Global variables
// Number of errors when lexing, counted in lexemes/words
int g_lexer_errors;
// Number of correct lexemes/words
int g_lexer_correct;

static void DoReadCharacter(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	*ch = fgetc(fp);
	if (*ch == NEWLINE)
		(*number_of_lines)++;
}

#define ReadCharacter(ch) DoReadCharacter(ch, fp, number_of_lines)

//! Consumes whitespace characters
static void avoidblanks(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	assert(ch);
	assert(fp);
	assert(number_of_lines);

	while (*ch != EOF) {
		if ((*ch != SPACE) && (*ch != TAB)) {
			if (*ch != NEWLINE)
				break;
			else
				(*number_of_lines)++;
		}
		ReadCharacter(ch);
	}
}

//! Consumes non-whitespace characters
static void avoidchars(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	assert(ch);
	assert(fp);

	WARN("(avoidchars) Found unrecognizable characters!\n");
	if (*ch != SPACE && *ch != NEWLINE && *ch != TAB) {
		while (*ch != EOF) {
			if (*ch != SPACE && *ch != NEWLINE && *ch != TAB)
				ReadCharacter(ch);
			else
				break;
		}
	}
}

#endif /* UTILS_H */
