/**
 * Group: MET4-4
 * Author: Nikolaos Renieris
 * Description: Defines the seek() function. For more refer to its
 * documentation.
 */

#ifndef SEEK_H
#define SEEK_H

#include "log.h"
#include <assert.h>

// Max number of characters that seek() is allowed to seek to
#define SEEK_LIMIT 8

//! Reads the character from `fp` at an offset of `index` characters forwards
static char seek(long long int index, FILE* fp)
{
	// Zero index doesn't make much sense (just reads the character at current
	// position)
	assert(index > 0);
	// We have an upper limit on the amount of characters we're allowed to seek
	assert(index <= SEEK_LIMIT);
	assert(fp);

	// Return value
	char read_char = '\0';

	// Keep previous position
	int prev_index = ftell(fp);

	// Read until we read the character we're interesed in
	while (index-- && read_char != EOF)
		read_char = fgetc(fp);

	// Return file pointer to where it was.
	fseek(fp, prev_index, SEEK_SET);
	LOG("Seeked character %c\n", read_char);

	return read_char;
}

// So that we don't have to include `fp` every time we call seek.
#define seek(index) seek(index, fp)

#endif /* SEEK_H */
