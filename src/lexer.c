/**
 * Group: MET4-4
 * Author: Nikolaos Renieris
           Alex Milas
 * Description: Entry point and skeleton of our Mini-Python lexer
 */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "seek.h"
#include "token/tokens.h"
#include "utils.h"

// entry point
int main(int argc, char** argv)
{
	char ch;
	FILE* fp;

	// argument checking
	if (argc <= 1) {
		puts("Usage:\n\tmpy_lexer <path_to_source_file>\n");
		exit(EXIT_FAILURE);
	}

	// open input source file
	LOG("Opening %s ...\n", argv[1]);

	fp = fopen(argv[1], "r");
	if (fp == NULL)
		return 1;

	// Total number of newlines (\n) in file
	unsigned int* number_of_lines = calloc(1, sizeof(unsigned int));
	// Add one to account for the first line (which lacks a newline character)
	(*number_of_lines)++;
	
	g_lexer_correct = 0;
	g_lexer_errors = 0;
	
	// read first character
	ReadCharacter(&ch);

	while (ch != EOF) {
		avoidblanks(&ch, fp, number_of_lines);
		// LOG_CH
		/* Anagnwrish xarakthra. Sto shmeio auto tha prepei na apofasistei
		   to eidos tou anagnwristikou (px.onoma metablhths,akeraios arithmos, string)
		   pou tha mporouse na arxizei apo ton xarakthra ch pou diabasthke kai akolouthws na
		   klhthei h synarthsh pou  saronei xarakthra-xarakthra tin pigaia symvoloseira kai
		   pragmatopoiei tin anagnorisei */
		switch (ch) {
		case '\'': {
			consume_string(&ch, fp, number_of_lines);
		} break;
		case '\"': {
			char ch1 = seek(1);
			if (ch1 == '\"') {
				// We read ""
				// We need to read one more character to determine whether
				// it's an empty string, or a multipline comment
				char ch2 = seek(2);

				if (ch2 == '\"')
				{
					consume_comment(&ch, fp, number_of_lines);
				}
				else
				{
					consume_string(&ch, fp, number_of_lines);
				}
			} else
				consume_string(&ch, fp, number_of_lines);
		} break;
		case '#': {
			// no need to pass number_of_lines, since a single line comment
			// can't include more than 1 line (thus we don't need to update
			// the value)
			consume_comment(&ch, fp, number_of_lines);
		} break;
		case 'A' ... 'Z':
		case 'a' ... 'z':
		case '_': {
			// no need to pass number_of_lines, since a variable is strictly
			// 1 line long, (thus we don't need to update the value)
			consume_variable(&ch, fp, number_of_lines);
		} break;
		case '0' ... '9':
		case '.': {
			// no need to pass number_of_lines, since a variable is strictly
			// 1 line long, (thus we don't need to update the value)
			consume_number(&ch, fp, number_of_lines);
		} break;
		case '+':
		case '-': {
			char c = seek(1);
			
			if ((c == '.') || (c >= '0' && c <= '9'))
			{
				// if the next character c is a type of number, start after c
				ReadCharacter(&ch);
				consume_number(&c, fp, number_of_lines);
			}
			else {
				INFO("type: OPERATOR  noc: 0  line: %d\n", number_of_lines);
				consume_operator(&ch, fp, number_of_lines);
				g_lexer_correct++;
			}

		} break;
		case '*':
		case '/':
		case '%':
		case '=':
		case '>':
		case '<':
		case '!': {
			// no need to pass number_of_lines, since an operator is strictly
			// 1 line long, (thus we don't need to update the value)
			consume_operator(&ch, fp, number_of_lines);
		} break;
		default: {
			// Non-recognisable character
			WARN("Unrecognisable character!\n");
			LOG_CH
			avoidchars(&ch, fp, number_of_lines);
		}
		}

		// read character for next iteration
		ReadCharacter(&ch);
	}

	fclose(fp);

	INFO("Finished. Number of lines: %u\n", *number_of_lines);
	
	INFO("Correct: %d\tWrong: %d", g_lexer_correct, g_lexer_errors);

	return EXIT_SUCCESS;
}
