/**
 * Group: MET4-4
 * Author: Alex Milas
 * Date: 12/11/2017
 * Description: Diadikasia anagnvrishs metavlhths
 */

#include "../log.h"
#include "../utils.h"
#include "tokens.h"

bool consume_variable(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	LOG("%s called. ch: %c [0x%02x]\n", __FUNCTION__, *ch, *ch);

	bool flag = false;
	unsigned int bad_chars = 0;
	unsigned int str_counter = 0;
	unsigned int counter = 0;
	unsigned int i = 0;

	enum State { S0, S1, GOOD, BAD, VAR };
	typedef enum State State;

	State currentState = S0;
	State prevState;

	while (1) {
		switch (currentState) {
		case S0: {
			if ((*ch >= 'A' && *ch <= 'Z') || (*ch >= 'a' && *ch <= 'z') || *ch == '_') {
				currentState = S1;
				prevState = S0;
				break;
			} else {
				currentState = BAD;
				prevState = S0;
				break;
			}
		}
		case S1: {
			if ((*ch >= 'A' && *ch <= 'Z') || (*ch >= 'a' && *ch <= 'z') || *ch == '_'
			    || (*ch >= '0' && *ch <= '9')) {
				currentState = S1;
				prevState = S1;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) // separator
			{
				currentState = VAR;
				prevState = S1;
				break;
			} else {
				currentState = BAD;
				prevState = S1;
				break;
			}
		}
		case VAR: // epitixis anagnorisi tou onomatos metavliths
		{
			currentState = GOOD;
			prevState = VAR;
			str_counter++;
			INFO("type: VARIABLE  noc: %d  line: %d\n", counter - 2, *number_of_lines);
			g_lexer_correct++;
			// system("pause");
			break;
		}
		case GOOD: {
			flag = true; // an to onoma metavliths threi ta
			g_lexer_correct++;
			// kritiria, alla3e to
			// flag se 1
			break;
		}
		case BAD: {
			prevState = BAD;
			bad_chars += counter;
			ERROR("bad VARIABLE!\n");
			g_lexer_errors++;
			avoidchars(ch, fp, number_of_lines);
			break;
		}
		}
		if ((currentState == GOOD) || ((currentState == BAD) && (prevState == BAD)))
			break;
		else if (currentState != VAR && currentState != GOOD && currentState != BAD) {
			i++;
			counter++;
			ReadCharacter(ch);
		}
	}

	return flag;
}
