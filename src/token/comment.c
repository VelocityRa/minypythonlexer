/**
 * Group: MET4-4
 * Authors: Alex Milas
            Nikolaos Renieris
 * Date: 12/11/2017
 * Description: Diadikasia anagnwrishs sxoliwn (single-line & multi-line)
 */

#include "../log.h"
#include "../utils.h"
#include "tokens.h"

/**
 * Consumes single-line and multi-line comments.
 *
 * @param ch  Pointer to current character.
 *        fp  File handle to the currently opened source file.
 *        is_multi_line  If this is true, the comment we're about to process is
 *                       supposed to be multi-line ("""...) otherwise it's
 *                       single line (#...).
 *        number_of_lines  Total number of lines in source code.
 *                         This function can add to it as it reads newlines
 *                         Valid only when is_multi_line is true.
 * @return  True if comment was successfully consumed, false otherwise.
 */
bool consume_comment(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	LOG("%s called. ch: %c [0x%02x]\n", __FUNCTION__, *ch, *ch);

	bool flag = false;
	unsigned int bad_chars = 0;
	unsigned int str_counter = 0;
	unsigned int counter = 0;
	unsigned int i = 0;

	enum State {
		S0, // S0 == kombos ekkinhshs
		S1, // S1, S2 == komboi gia ta sxolia mias grammhs
		S2,
		S4, // S4...S8 == komboi gia ta sxolia pollwn grammwn
		S5,
		S6,
		S7,
		S8,
		GOOD,
		BAD,
		COMM
	};
	typedef enum State State;

	State prevState;
	State currentState = S0;

	while (1) {
		switch (currentState) {
		case S0: {
			if (*ch == '#') // an einai single-line comment
			{
				currentState = S1;
				prevState = S0;
				break;
			} else if (*ch == '\"') {
				currentState = S4;
				prevState = S0;
				break;
			} else {
				currentState = BAD;
				prevState = S0;
				break;
			}
		}
		case S1: {
			if (*ch >= ' ' && *ch <= '~') // synolo olwn twn ektypwsimwn xarakthrwn
			{
				currentState = S1;
				prevState = S1;
				break;
			} else if (*ch == '\n') // ta sxolia mias grammhs
			// teleiwnoun me
			// thn allagh
			// pros thn amesws epomenh grammh
			{
				currentState = S2;
				prevState = S1;
				break;
			} else {
				currentState = BAD;
				prevState = S1;
				break;
			}
		}
		case S2: {
			if (*ch == EOF) {
				currentState = COMM;
				prevState = S2;
				break;
			}
		}
		case S4: {
			if (*ch == '\"') {
				currentState = S5;
				prevState = S4;
				break;
			} else {
				currentState = BAD;
				prevState = S4;
				break;
			}
		}
		case S5: {
			if (*ch == '\"') {
				currentState = S6;
				prevState = S5;
				break;
			} else if (*ch == 10 || (*ch >= ' ' && *ch <= '~')) // newline + synolo
			// olwn twn
			// ektypwsimwn
			// xarakthrwn
			{
				currentState = S5;
				prevState = S5;
				break;
			} else {
				currentState = BAD;
				prevState = S5;
				break;
			}
		}
		case S6: {
			if (*ch == '\"') {
				currentState = S7;
				prevState = S6;
				break;
			} else if (*ch == 10 || (*ch >= ' ' && *ch <= '~')) // newline + synolo
			// olwn twn
			// ektypwsimwn
			// xarakthrwn
			{
				currentState = S5;
				prevState = S6;
				break;
			} else {
				currentState = BAD;
				prevState = S6;
				break;
			}
		}
		case S7: {
			if (*ch == '\"') {
				currentState = S8;
				prevState = S7;
				break;
			} else if (*ch == 10 || (*ch >= ' ' && *ch <= '~')) // newline + synolo
			// olwn twn
			// ektypwsimwn
			// xarakthrwn
			{
				currentState = S5;
				prevState = S7;
				break;
			} else {
				currentState = BAD;
				prevState = S6;
				break;
			}
		}
		case S8: {
			if (*ch == '\n' || *ch == EOF) {
				currentState = COMM;
				prevState = S8;
				break;
			} else {
				currentState = BAD;
				prevState = S8;
				break;
			}
		}
		case COMM: // epitixis anagnorisi tou sxoliou mias grammis
		{
			currentState = GOOD;
			prevState = COMM;
			str_counter++;

			// TODO: add correct comment about - 2
			INFO("type: COMMENT\tnoc: %d  line: %d\n", counter - 2, *number_of_lines);
			g_lexer_correct++;
			// system("pause");
			break;
		}
		case GOOD: {
			// an to onoma metavliths threi ta kritiria, alla3e to flag se 1
			flag = true;
			g_lexer_correct++;
			break;
		}
		case BAD: {
			prevState = BAD;
			bad_chars += counter;
			ERROR("bad COMMENT!\n");
			g_lexer_errors++;
			avoidchars(ch, fp, number_of_lines);
			break;
		}
		}

		if ((currentState == GOOD) || ((currentState == BAD) && (prevState == BAD)))
			break;
		else if (currentState != COMM && currentState != GOOD && currentState != BAD) {
			i++;
			counter++;
			ReadCharacter(ch);
		}
	}

	return flag;
}
