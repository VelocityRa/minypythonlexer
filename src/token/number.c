/**
 * Group: MET4-4
 * Author: Alex Milas
 * Date: 19/11/2017
 * Description: Defines a function that recognizes whether a number is written
 * correctly based on the standards of Mini-Python
 */

#include "../log.h"
#include "../utils.h"
#include "tokens.h"

bool consume_number(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	LOG("%s called. ch: %c [0x%02x]\n", __FUNCTION__, *ch, *ch);

	bool flag = false;
	unsigned int bad_chars = 0;
	unsigned int str_counter = 0;
	unsigned int counter = 0;
	unsigned int i = 0;

	enum State { S0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, GOOD, BAD, NUMBER };
	typedef enum State State;

	State currentState = S0;
	State prevState;

	while (1) {
		switch (currentState) {
		case S0: {
			if (*ch == '.') {
				currentState = S1;
				prevState = S0;
				break;
			} else if (*ch == '0') {
				currentState = S2;
				prevState = S0;
				break;
			} else if (*ch >= '1' && *ch <= '9') {
				currentState = S3;
				prevState = S0;
				break;
			} else {
				currentState = BAD;
				prevState = S0;
				break;
			}
		}
		case S1: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S4;
				prevState = S0;
				break;
			} else {
				currentState = BAD;
				prevState = S1;
				break;
			}
		}
		case S2: {
			if (*ch == '0') {
				currentState = S2;
				prevState = S2;
				break;
			} else if (*ch == '.') {
				currentState = S6;
				prevState = S2;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S2;
				break;
			} else {
				currentState = BAD;
				prevState = S2;
				break;
			}
		}
		case S3: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S5;
				prevState = S3;
				break;
			} else if (*ch == '.') {
				currentState = S6;
				prevState = S3;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S3;
				break;
			} else {
				currentState = BAD;
				prevState = S3;
				break;
			}
		}
		case S4: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S4;
				prevState = S4;
				break;
			} else if (*ch == 'e' || *ch == 'E') {
				currentState = S8;
				prevState = S4;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S4;
				break;
			} else {
				currentState = BAD;
				prevState = S4;
				break;
			}
		}
		case S5: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S5;
				prevState = S5;
				break;
			} else if (*ch == '.') {
				currentState = S6;
				prevState = S5;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S5;
				break;
			} else {
				currentState = BAD;
				prevState = S5;
				break;
			}
		}
		case S6: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S7;
				prevState = S6;
				break;
			} else if (*ch == 'e' || *ch == 'E') {
				currentState = S8;
				prevState = S6;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S6;
				break;
			} else {
				currentState = BAD;
				prevState = S6;
				break;
			}
		}
		case S7: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S7;
				prevState = S7;
				break;
			} else if (*ch == 'e' || *ch == 'E') {
				currentState = S8;
				prevState = S7;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S7;
				break;
			} else {
				currentState = BAD;
				prevState = S7;
				break;
			}
		}
		case S8: {
			if (*ch == '+' || *ch == '-') {
				currentState = S9;
				prevState = S8;
				break;
			} else if (*ch >= '0' && *ch <= '9') {
				currentState = S10;
				prevState = S8;
				break;
			} else {
				currentState = BAD;
				prevState = S8;
				break;
			}
		}
		case S9: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S10;
				prevState = S9;
				break;
			} else {
				currentState = BAD;
				prevState = S9;
				break;
			}
		}
		case S10: {
			if (*ch >= '0' && *ch <= '9') {
				currentState = S10;
				prevState = S10;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = NUMBER;
				prevState = S10;
				break;
			} else {
				currentState = BAD;
				prevState = S10;
				break;
			}
		}
		case NUMBER: // successfull scanning of a number
		{
			currentState = GOOD;
			prevState = NUMBER;
			str_counter++;
			INFO("type: NUMBER  noc: %d  line: %d\n", counter - 2, *number_of_lines);
			g_lexer_correct++;
			// system("pause");
			break;
		}
		case GOOD: {
			flag = true;
			g_lexer_correct++;
			break;
		}
		case BAD: {
			prevState = BAD;
			bad_chars += counter;
			ERROR("bad NUMBER!\n");
			g_lexer_errors++;
			avoidchars(ch, fp, number_of_lines);
			break;
		}
		}

		if ((currentState == GOOD) || ((currentState == BAD) && (prevState == BAD)))
			break;
		else if (currentState != NUMBER && currentState != GOOD && currentState != BAD) {
			i++;
			counter++;
			ReadCharacter(ch);
		}
	}

	return flag;
}
