/**
 * Group: MET4-4
 * Author: Alex Milas
 * Date: 19/11/2017
 * Description: a function that recognizes operators such as +, -, .... based on
 * the standards of mini python
 */

#include "../log.h"
#include "../utils.h"
#include "tokens.h"

bool consume_operator(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	LOG("%s called. ch: %c [0x%02x]\n", __FUNCTION__, *ch, *ch);

	bool flag = false;
	unsigned int bad_chars = 0;
	unsigned int str_counter = 0;
	unsigned int counter = 0;
	unsigned int i = 0;

	enum State { S0, S1, S2, S3, S4, S5, S6, S7, S8, S9, S10, S11, S12, GOOD, BAD, OP };
	typedef enum State State;

	State currentState = S0;
	State prevState;

	while (1) {
		switch (currentState) {
		case S0: {
			if (*ch == '+') {
				currentState = S1;
				prevState = S0;
				break;
			} else if (*ch == '-') {
				currentState = S2;
				prevState = S0;
				break;
			} else if (*ch == '*') {
				currentState = S3;
				prevState = S0;
				break;
			} else if (*ch == '/') {
				currentState = S4;
				prevState = S0;
				break;
			} else if (*ch == '%') {
				currentState = S5;
				prevState = S0;
				break;
			} else if (*ch == '=') {
				currentState = S6;
				prevState = S0;
				break;
			} else if (*ch == '>') {
				currentState = S7;
				prevState = S0;
				break;
			} else if (*ch == '<') {
				currentState = S8;
				prevState = S0;
				break;
			} else if (*ch == '!') {
				currentState = S9;
				prevState = S0;
				break;
			} else {
				currentState = BAD;
				prevState = S0;
				break;
			}
		}
		case S1: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S1;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S1;
				break;
			} else {
				currentState = BAD;
				prevState = S1;
				break;
			}
		}
		case S2: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S2;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S2;
				break;
			} else {
				currentState = BAD;
				prevState = S2;
				break;
			}
		}
		case S3: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S3;
				break;
			} else if (*ch == '*') {
				currentState = S10;
				prevState = S3;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S3;
				break;
			} else {
				currentState = BAD;
				prevState = S3;
				break;
			}
		}
		case S4: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S4;
				break;
			} else if (*ch == '/') {
				currentState = S11;
				prevState = S4;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S4;
				break;
			} else {
				currentState = BAD;
				prevState = S4;
				break;
			}
		}
		case S5: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S5;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S5;
				break;
			} else {
				currentState = BAD;
				prevState = S5;
				break;
			}
		}
		case S6: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S6;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S6;
				break;
			} else {
				currentState = BAD;
				prevState = S6;
				break;
			}
		}
		case S7: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S7;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S7;
				break;
			} else {
				currentState = BAD;
				prevState = S7;
				break;
			}
		}
		case S8: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S8;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S8;
				break;
			} else {
				currentState = BAD;
				prevState = S8;
				break;
			}
		}
		case S9: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S9;
				break;
			} else {
				currentState = BAD;
				prevState = S9;
				break;
			}
		}
		case S10: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S10;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S10;
				break;
			} else {
				currentState = BAD;
				prevState = S10;
				break;
			}
		}
		case S11: {
			if (*ch == '=') {
				currentState = S12;
				prevState = S11;
				break;
			} else if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S11;
				break;
			} else {
				currentState = BAD;
				prevState = S11;
				break;
			}
		}
		case S12: {
			if (*ch == ' ' || *ch == '\n' || *ch == '\t' || *ch == EOF) {
				currentState = OP;
				prevState = S12;
				break;
			} else {
				currentState = BAD;
				prevState = S12;
				break;
			}
		}
		case OP: {
			currentState = GOOD;
			prevState = OP;
			str_counter++;
			g_lexer_correct++;
			INFO("type: OPERATOR  noc: %d  line: %d\n", counter, *number_of_lines);
			// system("pause");
			break;
		}
		case GOOD: {
			flag = true;
			g_lexer_correct++;
			break;
		}
		case BAD: {
			prevState = BAD;
			bad_chars += counter;
			ERROR("bad OPERATOR!\n");
			g_lexer_errors++;
			avoidchars(ch, fp, number_of_lines);
			break;
		}
		}

		if ((currentState == GOOD) || ((currentState == BAD) && (prevState == BAD)))
			break;
		else if (currentState != OP && currentState != GOOD && currentState != BAD) {
			i++;
			counter++;
			ReadCharacter(ch);
		}
	}

	return flag;
}
