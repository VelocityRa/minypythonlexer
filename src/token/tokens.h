/**
 * Group: MET4-4
 * Author: Nikolaos Renieris
 * Description: Exposes the lexer's consume functions
 */

#ifndef TOKENS_H
#define TOKENS_H

#include <stdbool.h>
#include <stdio.h>

bool consume_string(char* ch, FILE* fp, unsigned int* number_of_lines);
bool consume_comment(char* ch, FILE* fp, unsigned int* number_of_lines);
bool consume_variable(char* ch, FILE* fp, unsigned int* number_of_lines);
bool consume_operator(char* ch, FILE* fp, unsigned int* number_of_lines);
bool consume_number(char* ch, FILE* fp, unsigned int* number_of_lines);

#endif /* TOKENS_H */
