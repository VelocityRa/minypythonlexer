/**
 * Group: MET4-4
 * Author: Alex MIlas
 * Date: 12/11/2017
 * Description:
 */

#include "../log.h"
#include "../utils.h"
#include "tokens.h"

bool consume_string(char* ch, FILE* fp, unsigned int* number_of_lines)
{
	LOG("%s called. ch: %c [0x%02x]\n", __FUNCTION__, *ch && 0xFF, *ch && 0xFF);

	bool flag = false;
	unsigned int str_counter = 0;
	unsigned int counter = 0;
	unsigned int bad_chars = 0;
	unsigned int i = 0;

	enum State { S0, S1, S2, S3, S4, S5, GOOD, BAD, STRING };
	typedef enum State State;

	State currentState = S0;
	State prevState = S0;
	while (1) {
		switch (currentState) {
		case S0: {
			if (*ch == '\'') {
				currentState = S1;
				prevState = S0;
				break;
			} else if (*ch == '\"') {
				currentState = S3;
				prevState = S0;
				break;
			} else {
				currentState = BAD;
				prevState = S0;
				break;
			}
		}
		case S1: {
			if (*ch == 32 || *ch == 33 || (*ch >= 35 && *ch <= 38) || (*ch >= 40 && *ch <= 126)) {
				// oloi oi ektypwsimoi xarakthres(A-Z, a-z,
				// 0-9, keno,
				// ~!@#$...) ektos
				// apo ta ' kai "
				currentState = S1;
				prevState = S1;
				break;
			} else if (*ch == '\\') {
				currentState = S2;
				prevState = S1;
				break;
			} else if (*ch == '\'') {
				// epityxes string me single quotes
				currentState = S5;
				prevState = S1;
				break;
			} else {
				currentState = S1;
				prevState = S1;
				break;
			}
		}
		case S2: {
			if (*ch == 32 || *ch == 33 || (*ch >= 35 && *ch <= 126)) {
				// oloi oi ektypwsimoi xarakthres(A-Z, a-z,
				// 0-9, keno,
				// ~!@#$...) ektos
				// apo to "
				currentState = S1;
				prevState = S2;
				break;
			} else {
				currentState = BAD;
				prevState = S2;
				break;
			}
		}
		case S3: {
			if (*ch == 32 || *ch == 33 || (*ch >= 35 && *ch <= 38) || (*ch >= 40 && *ch <= 126)) {
				// oloi oi ektypwsimoi xarakthres(A-Z, a-z,
				// 0-9, keno,
				// ~!@#$...) ektos
				// apo ta ' kai "
				currentState = S3;
				prevState = S3;
				break;
			} else if (*ch == '\\') {
				currentState = S4;
				prevState = S3;
				break;
			} else if (*ch == '\"') {
				// epityxes string me single quotes
				currentState = S5;
				prevState = S3;
				break;
			} else {
				currentState = S1;
				prevState = S3;
				break;
			}
		}
		case S4: {
			if ((*ch >= 32 && *ch <= 38) || (*ch >= 40 && *ch <= 126)) {
				// oloi oi ektypwsimoi xarakthres(A-Z, a-z,
				// 0-9, keno,
				// ~!@#$...) ektos
				// apo to '
				currentState = S3;
				prevState = S4;
				break;
			} else {
				currentState = BAD;
				prevState = S4;
				break;
			}
		}
		case S5: {
			currentState = STRING;
			prevState = S5;
			break;
		}
		case STRING: {
			// epitixis anagnorisi enos string
			currentState = GOOD;
			prevState = STRING;
			str_counter++;
			INFO("type: STRING  noc: %d  line: %d\n", counter - 2, *number_of_lines);
			g_lexer_correct++;
			// system("pause");
			break;
		}
		case GOOD: {
			// an to string threi ta kritiria, alla3e to flag se 1
			g_lexer_correct++;
			flag = true;
			break;
		}
		case BAD: {
			prevState = BAD;
			bad_chars += counter;
			ERROR("bad STRING!\n");
			g_lexer_errors++;
			avoidchars(ch, fp, number_of_lines);
			break;
		}
		}
		if ((currentState == GOOD) || ((currentState == BAD) && (prevState == BAD)))
			break;
		else if (currentState != STRING && currentState != GOOD && currentState != BAD) {
			i++;
			counter++;
			ReadCharacter(ch);
		}
	}

	return flag;
}
