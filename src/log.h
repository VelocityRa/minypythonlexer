/**
 * Group: MET4-4
 * Author: Nikolaos Renieris
 * Description: Defines a few simple logging macros
 */

// comment this out to disable debug mode
#define _DEBUG

#ifndef LOG_H
#define LOG_H

// simple logging macros
// only enabled on debug mode
#ifdef _DEBUG

// The following 3 lines define the LOCATION macro, which contains
// the file and the line where logging functions get called from
#define S1(x) #x
#define S2(x) S1(x)
#define LOCATION "[[" __FILE__ " : " S2(__LINE__) "]]\t"

#define LOG(...) (printf("[LOG]:\t" LOCATION __VA_ARGS__))
#define INFO(...) (printf("[INFO]:\t" LOCATION __VA_ARGS__))
#define WARN(...) (printf("[WARN]:\t" LOCATION __VA_ARGS__))
#define ERROR(...) (printf("[ERROR]:\t" LOCATION __VA_ARGS__))

#define LOG_CH                                                                                     \
	do {                                                                                           \
		LOG("ch: %c [0x%02x]\n", ch, ch);                                                          \
	} while (0);
#else
#define LOG(...)
#define INFO(...)
#define WARN(...)
#define ERROR(...)

#define LOG_CH
#endif

#endif /* LOG_H */
